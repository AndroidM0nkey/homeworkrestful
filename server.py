
from flask import Flask, jsonify, request
  
app = Flask(__name__)

users = {}

@app.route('/', methods = ['GET', 'POST'])
def home():
    if(request.method == 'GET'):
  
        data = "hello world"
        return jsonify({'data': data})

@app.route('/get_user', methods = ['GET'])
def get_users():
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        json = request.json
        ans = set()
        for name in json:
            if name in users:
                ans.add(users[name])
        return jsonify({'data': ans})
    return jsonify({'data': 'not ok'})

@app.route('/change_user', methods = ['PUT'])
def change_user():
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        json = request.json
        ans = set()
        for user in json:
            if user in users:
                users[user['name']] = user
        return jsonify({'data': ans})
    return jsonify({'data': 'not ok'})

@app.route('/new_user', methods = ['POST'])
def new_user():
    ans = set()
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        json = request.json
        for user in json:
            if user['name'] not in users:
                users[user['name']] = user
                ans.add(user)
        return jsonify({'data': ans})
    return jsonify({'data': 'not ok'})

@app.route('/delete_user', methods = ['DELETE'])
def delete_user():
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        json = request.json
        ans = set()
        for user in json:
            if user in users:
                users.pop(user['name'])
                ans.add(user)
        return jsonify({'data': ans})
    return jsonify({'data': 'not ok'})
  
  
if __name__ == '__main__':
  
    app.run(debug = True)