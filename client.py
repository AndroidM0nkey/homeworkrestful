import requests
import json
import time
import base64
  
URL = "http://127.0.0.1:5000" #can be changed on demand 
headers = {'Content-type': 'application/json'}

if __name__ == '__main__':
    while(True):
        print('Choose what you want to do: (add/change/get/delete)')
        action = input()
        if (action == 'add'):
            print('Input name, path to avatar, gender and email')
            name = input()
            path = input()
            gender = input()
            email = input()

            with open(path, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                data = {name, encoded_string, gender, email}
                r = requests.post(url = (URL + '/new_user'), headers = headers, data = json.dumps(data))
                #it works with one profile at a time, but server's api can handle multiple profiles
        if (action == 'change'):
            print('Input name, path to avatar, gender and email')
            name = input()
            path = input()
            gender = input()
            email = input()

            with open(path, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                data = {name, encoded_string, gender, email}
                r = requests.put(url = (URL + '/change_user'), headers = headers, data = json.dumps(data))
                #it works with one profile at a time, but server's api can handle multiple profiles
        if (action == 'delete'):
            print('Input name, path to avatar, gender and email')
            name = input()
            path = input()
            gender = input()
            email = input()

            with open(path, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                data = {name, encoded_string, gender, email}
                r = requests.delete(url = (URL + '/delete_user'), headers = headers, data = json.dumps(data))
                #it works with one profile at a time, but server's api can handle multiple profiles
        if (action == 'get'):
            print('Input name, path to avatar, gender and email')
            name = input()
            path = input()
            gender = input()
            email = input()

            with open(path, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                data = {name, encoded_string, gender, email}
                r = requests.get(url = (URL + '/get_user'), headers = headers, data = json.dumps(data))
                #it works with one profile at a time, but server's api can handle multiple profiles
                print(r.json())
        time.sleep(1)






