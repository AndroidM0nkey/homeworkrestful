FROM python:3-onbuild
EXPOSE 5000
COPY server.py /
CMD ["python", "server.py"]
